window.urlSpace = 'server';
var URLs = {
	'local': {
		'vacancy': '../temp/jx/vacancy-item.html',
		'actions': '../temp/jx/actions.json',
		'shoplist': '../temp/jx/shops-filtered.json',
		'catCalc': '../temp/jx/calc-display.json',
		'shopFilter': '../temp/jx/shop-filter.html',
		'catalogFilter': '../temp/jx/catalog-filter.html',
		'shopDB': '../temp/jx/shops.json',
		'magazine': '../temp/jx/modal-magazine.html',
		'method': 'get',
		'raty': '#!',
		'ratyIMG': '../img/',
		'formCaptcha': '#!',
		'videoPluginPath': '../flash/',
		'catItemContent': '../temp/jx/catitem.json'
	},
	'server': {
		'vacancy': 'http://www.podrygka.ru/ajax/vacancy.php',
		'actions': 'http://www.podrygka.ru/ajax/action_more.php',
		'shoplist': 'http://www.podrygka.ru/ajax/shoplist.php',
		'catCalc': 'http://www.podrygka.ru/ajax/calculator.php',
		'shopFilter': 'http://www.podrygka.ru/ajax/shop-filter.php',
		'catalogFilter': 'http://www.podrygka.ru/ajax/catalog-filter.php',
		'shopDB': 'http://www.podrygka.ru/ajax/allshops.php',
		'magazine': 'http://www.podrygka.ru/ajax/magazine.php',
		'method': 'post',
		'raty': '#!',
		'ratyIMG': 'http://www.podrygka.ru/img/',
		'formCaptcha': 'http://www.podrygka.ru/captcha_validate.php',
		'catItemContent': 'http://www.podrygka.ru/ajax/catitem.php'
	}
};
var shopDB;
function ShopDB(shopList) {
	this.db = this.dbList = shopList;
	this.markerCollection = new ymaps.GeoObjectArray();
}
var workURLs = URLs[window.urlSpace || 'local'];
ymaps.ready(function() {
	ShopDB.prototype = $.extend(ShopDB.prototype, {
		'constructor': shopDB,
		'getDB': function(url) {
			var that = this;
			$.getJSON(url).done(function(raw) {
				that.db = raw['shops'];
				that.dbList = raw['shops-list'];
			}).fail(function() {
				console.info('Ошибка загрузки данных');
			});
		},
		'dbIVID': undefined,
		'filterIVID': 0,
		'filter': {},
		'streetAddress': '',
		filterObjects: function(obj, callback) {
			var result = {};
			for (var i in obj) {
				if (obj.hasOwnProperty(i) && callback(obj[i])) {
					result[i] = obj[i];
				}
			}
			return result;
		},
		clearFilter: function() {
			this.filter = {};
			return this;
		},
		setFilter: function(field, value) {
			if (value == 0) {
				delete this.filter[field];
			} else {
				this.filter[field] = value;
			}
			return this;
		},
		setStreet: function(adress) {
			this.streetAddress = adress;
			return this;
		},
		useFilter: function() {
			var shops = this.db;
			var value;
			for (var field in this.filter) {
				if (!this.filter.hasOwnProperty(field)) {
					continue;
				}
				value = this.filter[field];
				shops = this.filterObjects(shops, function(shop) {
					return typeof shop[field] == 'object' ? $.inArray(value.toString(), shop[field]) >= 0 : shop[field] == value;
				});
			}
			var shopId = $.map(shops, function(value, key) {
				return key
			});
			var useBounds = true;
			var map = this.map;
			var that = this;
			if (typeof this.searchMarkers != 'undefined') {
				map.geoObjects.remove(this.searchMarkers);
			}
			if (this.streetAddress.length && typeof this.filter.street == 'undefined') {
				ymaps.geocode(this.streetAddress, {kind: 'street'}).then(function(o) {
					that.searchMarkers = o.geoObjects;
					map.geoObjects.add(that.searchMarkers);
					map.setCenter(that.searchMarkers.get(0).geometry.getCoordinates(), 12, {'checkZoomRange': true});
				});
				useBounds = false;
			}
			this.filterMarkers(map, shopId, false, useBounds, true);
			$('.panel.list .shop-list .item').hide().addClass('hidden');
			$.each(shops, function(index, value) {
				$('.panel.list .shop-list .item[data-id="' + value.id + '"]').show().removeClass('hidden');
			});
			$(window).trigger('resize');
			return this;
		},
		'renderAll': function(mapObject, boundToMarkers, isVacancy, callback) {
			this.checkMap(mapObject);
			this.map = mapObject;
			var that = this;
			that.dbIVID = setInterval(function() {
				if (that.db) {
					clearInterval(that.dbIVID);
					var ifAjax = $('.shop-tab-view .panel.map').data('ajax') == 'Y' || $('#shop-page-map-object').data('ajax') == 'Y';
					$.each(that.db, function(property, value) {
						var shopMarker = null, markerOptions = $.extend({}, that.markerDefaults);
						markerOptions.balloonContentLayout = isVacancy ? that.balloonCustomLayoutVacancy : that.balloonCustomLayout;
						if (value.icon && value.new_shop > 0) {
							markerOptions.iconImageHref = 'http://www.podrygka.ru/img/marker-place.png';
							markerOptions.iconImageSize = [44, 50];
							markerOptions.iconOffset = [-9, -9];
							markerOptions.iconContentLayout = that.iconLayout;
							markerOptions.iconContentOffset = [4, 6];
							markerOptions.balloonImageOffset = [25, -220]
						} else if (value.icon) {
							markerOptions.iconImageHref = value.icon;
							markerOptions.icon = value.icon;
						}
						shopMarker = new ymaps.Placemark(value.coords, {
							'v': value,
							'shopId': property,
							'has_perfume': value.perfume > 0
						}, markerOptions);
						if (ifAjax) {
							shopMarker.events.add('click', function(e) {
								var placemark = e.get('target'), shop_id = placemark.properties.get('shopId');
								placemark.options.set("balloonContentLayout", ymaps.templateLayoutFactory.createClass('<div class="shop-list shop-list-balloon"><div class="item">Идет загрузка данных магазина... <br/>(настроить при переносе на боевой сервер)</div></div>'));
								setTimeout(function() {
									$.ajax('http://www.podrygka.ru/ajax/get_shop.php', {
										method: "post",
										data: {ID: shop_id, ajax: 1},
										dataType: 'html',
										success: function(html) {
											placemark.options.set("balloonContentLayout", ymaps.templateLayoutFactory.createClass(html))
										}
									});
								}, 250);
							});
						}
						that.markerCollection.add(shopMarker);
					});
					window.map_reload = false;
					mapObject.geoObjects.add(that.markerCollection);
					mapObject.events.add('balloonopen', function(e) {
						var balloon = e.get('balloon');
						mapObject.events.add('click', function(e) {
							if (e.get('target') === mapObject) {
								balloon.close();
							}
						});
					});
					if (boundToMarkers) {
						mapObject.setBounds(that.markerCollection.getBounds(), {'checkZoomRange': true});
					}
					if (typeof callback == 'function')callback(that);
				} else {
					console.warn('База магазинов не получена. Еще попытка...');
				}
			}, 500);
		},
		'filterMarkers': function(mapObject, idArray, onlyPassed, useBounds, noActiveNoClick) {
			this.checkMap(mapObject);
			if (!idArray) {
				throw'Не передан массив отфильтрованных магазинов';
			}
			var that = this;
			clearInterval(that.filterIVID);
			if (that.markerCollection.getLength()) {
				that.filterIVID = setInterval(function() {
					var collectionLength = that.markerCollection.getLength();
					if (collectionLength) {
						clearInterval(that.filterIVID);
						var cBounds = new ymaps.GeoObjectCollection();
						that.markerCollection.each(function(element, index) {
							var shopId = element.properties.get('shopId');
							var newIconUrl;
							var isVisible = true;
							var isClickable = true;
							if ($.inArray(shopId, idArray) !== -1) {
								newIconUrl = $.isFunction(element.options.get('iconContentLayout')) ? 'http://www.podrygka.ru/img/marker-place.png' : 'http://www.podrygka.ru/img/marker.png?v1';
								if (element.options.get('icon'))newIconUrl = element.options.get('icon');
								cBounds.add(new ymaps.Placemark(element.geometry.getCoordinates()));
							} else {
								newIconUrl = $.isFunction(element.options.get('iconContentLayout')) ? 'http://www.podrygka.ru/img/marker-place-noactive.png' : 'http://www.podrygka.ru/img/marker-noactive.png?v1';
								if (element.options.get('icon'))newIconUrl = element.options.get('icon');
								if (onlyPassed)isVisible = false;
								if (noActiveNoClick)isClickable = false;
							}
							element.options.set('iconImageHref', newIconUrl);
							element.options.set('visible', isVisible);
							element.options.set('openBalloonOnClick', isClickable);
							if (idArray.length && index == collectionLength - 1 && useBounds) {
								mapObject.geoObjects.add(cBounds);
								mapObject.setBounds(cBounds.getBounds(), {'checkZoomRange': true});
								mapObject.geoObjects.remove(cBounds);
							}
						});
					} else {
						console.warn('Маркеры карты не отображены. Еще попытка...');
					}
				}, 300);
			}
		},
		'checkMap': function(mapObject) {
			if (!mapObject) {
				throw('Не передан объект карты');
			}
		},
		'iconLayout': ymaps.templateLayoutFactory.createClass('<img src="$[properties.v.icon]" width="37" height="35" />'),
		'balloonCustomLayoutVacancy': ymaps.templateLayoutFactory.createClass('<div class="shop-list shop-list-balloon vacancy-balloon"><div class="item">' + '<span class="close-balloon" title="Закрыть"></span>' + '<div class="title">' + '<a target="_blank" href="$[properties.v.link]">' + '$[properties.v.shop]' + '<em>$[properties.v.title]</em>' + '</a>' + '</div>' + '[if properties.v.address]' + '<div class="address">' + '$[properties.v.address]' + '</div>' + '[endif]' + '<dl class="contacts">' + '[if properties.v.metro]' + '<dt>Метро</dt>' + '<dd>' + '<span class="metro"><i class="line" style="background-color: $[properties.v.metro.line];"></i>$[properties.v.metro.station]</span>' + '</dd>' + '[endif]' + '[if properties.v.phone]' + '<dt>Телефон</dt>' + '<dd>$[properties.v.phone]</dd>' + '[endif]' + '</dl>' + '[if properties.v.time]' + '<dl class="time">' + '<dt>Время работы</dt>' + '<dd>$[properties.v.time]</dd>' + '</dl>' + '[endif]' + '<div class="vacancy-formadd">' + '<span class="vacancy-formadd__button rainbow-button js-vacancy-formadd" data-alt="Убрать из анкеты" data-id="$[properties.v.id]">Добавить в анкету</span>' + '</div>' + '</div></div>'),
		'balloonCustomLayout': ymaps.templateLayoutFactory.createClass('<div class="shop-list shop-list-balloon"><div class="item">' + '<span class="close-balloon" title="Закрыть"></span>' + '<div class="title">' + '<a target="_blank" href="$[properties.v.link]">' + '$[properties.v.shop]' + '<em>$[properties.v.title]</em>' + '</a>' + '</div>' + '<a target="_blank" href="$[properties.v.link]">' + '<img class="photo_small" src="http://www.podrygka.ru/img/images/photo_small.png">' + '</a>' + '[if properties.v.address]' + '<div class="address">' + '$[properties.v.address]' + '</div>' + '[endif]' + '<dl class="contacts">' + '[if properties.v.metro]' + '<dt>Метро</dt>' + '<dd>' + '<span class="metro"><i class="line" style="background-color: $[properties.v.metro.line];"></i>$[properties.v.metro.station]</span>' + '</dd>' + '[endif]' + '[if properties.v.phone]' + '<dt>Телефон</dt>' + '<dd>$[properties.v.phone]</dd>' + '[endif]' + '</dl>' + '[if properties.v.time]' + '<dl class="time">' + '<dt>Время работы</dt>' + '<dd>$[properties.v.time]</dd>' + '</dl>' + '[endif]' + '<a target="_blank" class="link" href="$[properties.v.link]">Как пройти »</a>' + '</div></div>'),
		'markerDefaults': {
			'iconImageHref': 'http://www.podrygka.ru/img/marker.png',
			'iconImageSize': [54, 40],
			'iconOffset': [-4, 3],
			'zIndexActive': 700,
			'balloonImageSize': [344, 185],
			'balloonImageHref': 'http://www.podrygka.ru/img/balloon-bg.png',
			'balloonCloseButton': false,
			'balloonShadow': true,
			'hideIconOnBalloonOpen': false,
			'balloonImageOffset': [5, -205],
			'balloonAutoPan': true
		}
	});
	var vacancy = function() {
		var context, el, data;

		function init() {
			context = $('.vac-view');
			el = {
				'stickers': $('.vac-view-stickers-item', context),
				'cTip': $('.vac-view-content-tip', context),
				'cPort': $('.vac-view-content-port', context),
				'vacModal': $('.modal-vacancy')
			};
			data = {'firstEnter': true, 'vacancyPath': window.location.hash.substr(1).split('-')};
			var stickerIndex = 0;
			var defaultSticker = el.stickers.filter('[data-type="' + data.vacancyPath[0] + '"]');
			if (defaultSticker.length == 1) {
				stickerIndex = el.stickers.index(defaultSticker);
			}
			initShopSelector();
			el.stickers.on('click', loadSticker).eq(stickerIndex).click();
		}

		function changeItem(all, that, actClass) {
			var activeClass = actClass || 'active';
			all.removeClass(activeClass).filter(that).addClass(activeClass);
		}

		function initVacTabs() {
			var tabs = $('.vac-item-tabs', el.port), tabsLI = $('LI', tabs), items = $('LI > DIV', tabs), panels = $('.vac-item-panel', el.port);
			items.on('click', function() {
				var $that = $(this);
				if (!$that.hasClass('active')) {
					var relPanel = panels.filter('[data-panel="' + $that.data('panel') + '"]');
					changeItem(items, $that);
					changeItem(panels, relPanel);
				}
			});
			if (items.length > 1 && data.firstEnter) {
				data.firstEnter = false;
				var tabIndex = 0;
				if (data.vacancyPath[1]) {
					var targetTab = tabsLI.filter('[data-id="' + data.vacancyPath[1] + '"]');
					if (targetTab.length == 1) {
						tabIndex = tabsLI.index(targetTab);
					}
				}
				tabsLI.eq(tabIndex).find(items).trigger('click');
			} else {
				items.first().trigger('click');
			}
		}

		function initVacDrops() {
			var dropContext = $(this), dropTitles = $('DT > SPAN', dropContext), dropPanels = $('DD', dropContext);
			dropTitles.on('click', function() {
				var $that = $(this), relpanel = dropPanels.filter('[data-panel="' + $that.data('panel') + '"]');
				$that.toggleClass('opened');
				relpanel.stop().slideToggle(200);
			})
		}

		function initVacMapObject(mapID, mapData) {
			var mapObject = null;
			if (mapID && mapData) {
				mapObject = new ymaps.Map(mapID, mapData);
			}
			return mapObject;
		}

		function initVacMap() {
			var locators = $('.vac-item-locator');
			$.each(locators, function() {
				var thatLocator = $(this), shopChooser = $('.vac-item-locator-shop > SPAN', thatLocator), mapHolder = $('.vac-item-locator-map', thatLocator), mapBox = $('.vac-item-locator-map-object', mapHolder), citySelect = thatLocator.find('.vac-item-locator-shop-city SELECT'), mapObject = null;
				shopChooser.on('click', function() {
					if (!thatLocator.hasClass('open')) {
						thatLocator.addClass('open');
						mapHolder.stop().slideDown(function() {
							if (!thatLocator.data('isMapInit')) {
								var mapID = mapBox.attr('id'), mapData = {
									'center': [mapBox.data('lat'), mapBox.data('lon')],
									'zoom': parseInt(mapBox.data('zoom'), 10)
								};
								citySelect.dropkick({
									'theme': 'simple', 'change': function(value, label) {
										window.shopDB[mapID].setFilter('city', parseInt(value)).useFilter();
										$('.js-vacancy-city').val(label);
									}
								});
								setTimeout(function() {
									citySelect.closest('.shop-select').find('.dk_options_scroller').baron({
										'track': '.dk_options_scroller-track',
										'bar': '.dk_options_scroller-track-bar',
										'barOnCls': 'baron-ready'
									});
								}, 100);
								thatLocator.data('isMapInit', true);
								mapObject = initVacMapObject(mapID, mapData);
								mapObject.controls.add('zoomControl', {left: '12px', top: '40px'});
								window.shopDB[mapID].renderAll(mapObject, false, true);
							}
						});
					} else {
						thatLocator.removeClass('open');
						mapHolder.stop().slideUp();
					}
				});
			});
		}

		function initModal() {
			var modalLinks = $('.modal-link[data-modal="vacancy"]', el.cPort);
			var modalHeader = $('H2', el.vacModal);
			var modalHidden = $('.js-modal-vacancy-name', el.vacModal);
			modalLinks.each(function() {
				var $that = $(this);
				$that.on('click', function() {
					var headerTitle = 'Анкета вакансии<br/>' + $that.data('vacancyName');
					modalHeader.html(headerTitle);
					$('.modal-vac-success').find(modalHeader).html(headerTitle + ',<br />отправлена');
					modalHidden.val($that.data('vacancyName'));
				});
			});
		}

		function fillVacPort(raw) {
			el.cPort.html(raw);
			if (el.cPort.find('.vac-item-tabs')) {
				$('.vac-item-tabs').each(initVacTabs);
			}
			if (el.cPort.find('.vac-item-drops')) {
				$('.vac-item-drops').each(initVacDrops);
			}
			if (el.cPort.find('.vac-item-locator-shop')) {
				initVacMap();
			}
			if (el.cPort.find('.modal-link[data-modal="vacancy"]')) {
				initModal();
			}
		}

		function positionTip(sticker) {
			var stickerOffset = sticker.position().left;
			el.cTip.css({'left': stickerOffset + (sticker.width() / 2) - 60});
		}

		function drawStickerStroke(sticker) {
			var pars = {'duration': 350, 'frames': 15, 'current': 0, 'strokeIV': undefined, 'frameWidth': 238};
			if (sticker.hasClass('active')) {
				var stroke = $('.vac-view-stickers-item-stroke', sticker);
				stroke.css('backgroundPosition', '-200% 0');
				pars.strokeIV = setInterval(function() {
					stroke.css('backgroundPosition', '-' + pars.current++ * pars.frameWidth + 'px 0');
					if (pars.current == pars.frames) {
						clearInterval(pars.strokeIV);
					}
				}, pars.duration / pars.frames);
			}
		}

		function loadSticker() {
			var $that = $(this), thatData = $that.data();
			if (!thatData['current'] && !$that.hasClass('active')) {
				positionTip($that);
				el.stickers.removeClass('active').filter($that).addClass('active');
				drawStickerStroke($that);
				el.cPort.addClass('loading');
				$.get(workURLs.vacancy, {'type': thatData['type']}).success(function(raw) {
					fillVacPort(raw);
				}).error(function() {
					console.info('Ошибка загрузки данных');
				}).complete(function() {
					el.cPort.removeClass('loading');
				});
			}
		}

		function initShopSelector() {
			var balloonButtonSelector = '.js-vacancy-formadd';
			var selItemSelector = '.js-shop-selector-item';
			var shopSelector = $('.js-shop-selector');
			var shopSelectorList = $('.js-shop-selector-list', shopSelector);
			var shopSelectorPass = $('.js-shop-selector-pass', shopSelector);
			var checkedShops = [];

			function updateShopSelector() {
				var filteredShops = $.unique(checkedShops);
				var listExport = [];
				var passVal = [];
				var mapID = $('.vac-item-panel.active .vac-item-locator-map-object').attr('id');
				$.each(filteredShops, function() {
					var shopID = this;
					var shopData = window.shopDB[mapID].db[shopID];
					var itemTemplate = ['<li class="shop-selector__item js-shop-selector-item"'];
					if (shopData) {
						itemTemplate.push('data-id="');
						itemTemplate.push(shopID);
						itemTemplate.push('">');
						itemTemplate.push(shopData['title']);
						itemTemplate.push('<i class="shop-selector__item-remove js-shop-selector-item-remove">&times;</i>');
						itemTemplate.push('</li>');
						passVal.push(shopData['title']);
					}
					listExport.push(itemTemplate.join(''));
				});
				shopSelectorList.html(listExport.join(''));
				shopSelectorPass.val(passVal.join(';'));
				if (filteredShops.length) {
					shopSelector.show();
				} else {
					shopSelector.hide();
				}
			}

			$(document).on('click', selItemSelector, function() {
				var $that = $(this);
				if ($that.hasClass('_add')) {
					el.vacModal.addClass('_asided');
				} else {
					$that.addClass('_removed');
					checkedShops = $.grep(checkedShops, function(value) {
						return value != $that.data('id');
					});
				}
				updateShopSelector();
			});
			context.on('click', balloonButtonSelector, function() {
				var $that = $(this);
				var oldText = $that.text();
				if ($that.data('checked')) {
					$that.removeClass('_checked');
					$that.data('checked', false);
					checkedShops = $.grep(checkedShops, function(value) {
						return value != $that.data('id');
					});
				} else {
					$that.addClass('_checked');
					$that.data('checked', true);
					checkedShops.push($that.data('id'));
				}
				$that.text($that.data('alt')).data('alt', oldText);
				updateShopSelector();
			});
			updateShopSelector();
		}

		return {init: init}
	}();
	window.shopList = function() {
		var context, el, data;

		function init() {
			context = $('.shop-view');
			el = {'filter': $('.shop-filter', context), 'tab': $('.shop-tab-view', context)};
			data = {'map': null, 'dbAccessIVID': null};
			if (el.tab.length) {
				if ($('#shopPopup')) {
					$.extend(el, {
						'tabTriggers': $('> .tabs LI', el.tab),
						'tabPanels': $('.shop-result-canvas > .panel', el.tab),
						'shopMap': $('.shop-map', el.tab),
						'shopList': $('.shop-list', el.tab)
					});
				} else {
					$.extend(el, {
						'tabTriggers': $('> .tabs LI', el.tab),
						'tabPanels': $('> .panel', el.tab),
						'shopMap': $('.shop-map', el.tab),
						'shopList': $('.shop-list', el.tab)
					});
				}
				el.tabTriggers.on('click', togglePanel);
				$(window).on('resize', winResize).trigger('resize');
				if (el.filter.length)bindFilter();
				initMap();
				data.dbAccessIVID = setInterval(function() {
					if (window.shopDB.db) {
						clearInterval(data.dbAccessIVID);
						var mapShops = [];
						var listShops = [];
						$.each(window.shopDB.db, function(prop) {
							mapShops.push(prop);
						});
						$.each(window.shopDB.dbList, function(prop) {
							listShops.push(prop);
						});
						fillData({'map': mapShops, 'list': $('#shopPopup').length ? window.sortInfo : listShops}, true);
					}
				}, 100);
			}
		}

		function bindFilter() {
			$.extend(el, {'filterSelect': $('.shop-select SELECT', el.filter)});
			el.filterSelect.each(initSelect);
			el.filterSelect.add($('input:checkbox', el.filter));
		}

		function fillData(raw, initFill) {
			var targetShops = [], listBuffer = '';
			$.each(raw.list, function(index, value) {
				var expShop = window.shopDB.dbList[value];
				if (expShop) {
					targetShops.push(expShop);
				}
			});
			var itemCounter = 0, oldLine = 0, lineOff = false, firstItem = false, tdCounter = 0;
			$.each(targetShops, function(property, value) {
				var listItem = '';
				if ($('#shopPopup').length && !lineOff) {
					var curLine = value.station.length ? value.station[0].line_id : 0;
					if (oldLine != curLine && curLine != 0) {
						listItem += '<div class="line-sorter" style="background-color: #' + value.station[0].color + ';"><span>' + value.station[0].line_name + '</span></div>';
						firstItem = true;
					} else if (curLine == 0 && itemCounter == 0) {
						lineOff = true;
					} else if (curLine == 0) {
						lineOff = true;
						listItem += '<div class="line-sorter" style="background-color: #ed0a95;"></div>';
						firstItem = true;
					}
				}
				if ($('#shopPopup').length) {
					if (tdCounter % 4 == 0 || firstItem) {
						firstItem = false;
						listItem += '<div class="item first" data-id="' + value.id + '">';
						tdCounter = 0;
					} else {
						listItem += '<div class="item" data-id="' + value.id + '">';
					}
				} else {
					listItem += '<div class="item" data-id="' + value.id + '">';
				}
				if (value.perfume > 0)listItem += '<div class="perfume">Есть парфюмерия</div>';
				listItem += '<div class="title"><a target="_blank" href="' + value.link + '">' + value.shop + '<em>' + value.title + '</em>' + '</a>' + '</div>';
				if (value.address && $('#shopPopup').length && !value.station.length && value.city != "Москва" && value.city != "Санкт-Петербург")listItem += '<div class="address">г. ' + value.city + ", " + value.address + '</div>'; else if (value.address)listItem += '<div class="address">' + value.address + '</div>';
				if (value.station || value.phone) {
					listItem += '<dl class="contacts">';
					if (value.station.length) {
						listItem += '<dt>Метро</dt>' + '<dd>';
						for (var i = 0; i < value.station.length; i++) {
							var metroLi = value.station[i];
							listItem += '<span class="metro">' + '<i class="line" style="background-color: #' + metroLi.color + ';"></i>' + metroLi.long_name + '</span>';
						}
						listItem += '</dd>';
					}
					if (value.phone)listItem += '<dt>Телефон</dt>' + '<dd>' + value.phone + '</dd>';
					listItem += '</dl>';
				}
				if (value.time) {
					listItem += '<dl class="time">';
					if (value.close > 0) {
						listItem += '<dt>Магазин закрыт</dt>';
					} else if (value.new_shop > 0 && value.days_open > 0) {
						listItem += '<dt>Новый магазин</dt>';
						listItem += '<dd>';
						listItem += '<table class="work-time">';
						var d = new Date(value.date_open), curr_date = d.getDate(), curr_month = d.getMonth() + 1, curr_year = d.getFullYear();
						if (curr_date < 10)curr_date = "0" + curr_date;
						if (curr_month < 10)curr_month = "0" + curr_month;
						listItem += '<tr><td>Дата открытия:</td><td>' + curr_date + '.' + curr_month + '.' + curr_year + '</td></tr>';
						listItem += '</table>';
						listItem += '</dd>';
					} else if (value.repair > 0) {
						listItem += '<dt>Магазин закрыт на реконструкцию</dt>';
					} else {
						var arDay = {
							'Mon': 'Понедельник',
							'Tue': 'Вторник',
							'Wed': 'Среда',
							'Thu': 'Четверг',
							'Fri': 'Пятница',
							'Sat': 'Суббота',
							'Sun': 'Воскресенье'
						};
						listItem += '<dt>Время работы</dt>';
						listItem += '<dd>';
						listItem += '<table class="work-time">';
						var workTime = "выходной";
						if (value.time.All) {
							workTime = 'с ' + value.time.All.time_from + ' до ' + value.time.All.time_to;
							listItem += '<tr><td>Ежедневно:</td><td>' + workTime + '</td></tr>';
						} else {
							$.each(arDay, function(key, val) {
								workTime = "выходной";
								if (value.time[key] && (value.time[key]['time_from'] != '00:00' || value.time[key]['time_to'] != '00:00')) {
									workTime = 'с ' + value.time[key]['time_from'] + ' до ' + value.time[key]['time_to'];
								}
								listItem += '<tr>';
								listItem += '<td>' + val + '</td>';
								listItem += '<td>' + workTime + '</td>';
								listItem += '</tr>';
							});
						}
						listItem += '</table>';
						listItem += '</dd>';
					}
					listItem += '</dl>';
				}
				listItem += '</div>';
				listBuffer += listItem;
				tdCounter++;
				if ($('#shopPopup').length) {
					oldLine = curLine;
					itemCounter++;
					if (itemCounter == 12 && itemCounter != targetShops.length) {
						listBuffer += '<div class="hidden-shop">';
					} else if (itemCounter == targetShops.length && itemCounter > 12) {
						listBuffer += '</div><div class="act-load-more"> <a href="#!" data-current="1"><i></i>Загрузить ещё</a> </div>';
					}
				}
			});
			el.shopList.html(listBuffer);
			shopDB.filterMarkers(data.map, raw.map, false, !initFill);
			$(window).trigger('resize');
		}

		function initMap() {
			var filterInData = $('.shop-filter-i', context).data();
			if ($("#shopPopup").length)filterInData = $("#shopPopup .shop-tab-view").data();
			data.map = new ymaps.Map('shops', {
				'center': [filterInData['lat'], filterInData['lon']],
				'zoom': filterInData['zoom']
			});
			data.map.controls.add('zoomControl', {left: '10px', top: '13px'});
			$('.shop-filter-i input[name=tabs]').on('click', function() {
				data.map.setCenter([$(this).data('lat'), $(this).data('lon')]);
			});
			window.shopDB.renderAll(data.map);
		}

		function initSelect() {
			var $that = $(this);
			$that.dropkick();
			setTimeout(function() {
				$that.closest('.dk_container').find('.dk_options_scroller').baron({
					'track': '.dk_options_scroller-track',
					'bar': '.dk_options_scroller-track-bar',
					'barOnCls': 'baron-ready'
				});
			}, 100);
		}

		function winResize(event) {
			if ($("#shopPopup").length)return false;
			var currentLayout = Modernizr.mq('only screen and (min-width:1500px)');
			if (currentLayout !== data.layout || event.isTrigger) {
				data.layout = currentLayout;
				var textItems = $('.item', el.shopList);
				el.shopList.toggleClass('wide', data.layout);
				textItems.removeClass('nomrg').filter(':not(.hidden)').filter(function(i) {
					return data.layout ? (i % 3 == 0) : (i % 2 == 0);
				}).addClass('nomrg');
			}
		}

		function togglePanel() {
			var $that = $(this);
			if (!$that.hasClass('active') && !$that.parent().hasClass('link-tab')) {
				var relPanel = el.tabPanels.filter(function() {
					return $that.data('panel') == $(this).data('panel');
				});
				$that.siblings().removeClass('active').end().addClass('active');
				relPanel.show();
				el.tabPanels.not(relPanel).hide();
			}
		}

		return {'init': init}
	}();
	$(function() {
		vacancy.init();
		(function() {
			var target = $('.l-plate-contacts');
			target.each(function() {
				var context = $(this), el = {
					'mapContainer': $('#contacts-map'),
					'branches': $('.contacts-branches', context),
					'branchTitles': $('.contacts-branches > DT A', context),
					'branchPanels': $('.contacts-branches > DD', context),
					'modalLinks': $('.modal-link', context),
					'modalContactInput': $('.modal-phone .callme-contact-id')
				}, data = {'mapContainerData': el.mapContainer.data()};

				function mapInit() {
					data.map = new ymaps.Map('contacts-map', {
						'center': data.mapContainerData.coords.split(','),
						'zoom': data.mapContainerData.zoom
					});
					data.map.controls.add('zoomControl', {left: '316px', top: '50px'});
					var mainMarker = new ymaps.Placemark(data.mapContainerData.coords.split(','), {}, {
						'iconImageHref': 'http://www.podrygka.ru/img/marker-contacts.png',
						'iconImageSize': [54, 40],
						'iconImageOffset': [-15, -36]
					});
					data.map.geoObjects.add(mainMarker);
				}

				function branchInit() {
					function toggleBranch() {
						var $that = $(this);
						$that.toggleClass('opened');
						$that.closest('DT').next('DD').stop().slideToggle(600);
					}

					el.branchTitles.on('click', toggleBranch);
				}

				mapInit();
				branchInit();
				el.modalLinks.on('click', function() {
					var $that = $(this);
					el.modalContactInput.val($that.data('contact'));
				});
			});
		})();
		(function() {
			var target = $('.l-plate-actions-page');
			target.each(function() {
				var context = $(this), el = {
					'shops': $('.action-shops', context),
					'shopsTwin': $('.action-shops-twin', context),
					'shopsList': $('.action-shops-list', context),
					'shopsItems': $('.action-shops-list .item', context),
					'shopsMap': $('.action-shops-map', context),
					'toggleList': $('.action-shops-more SPAN', context),
					'shareList': $('.js-catitem-social-buttons', context)
				}, data = {'map': null, 'scrollerHeight': 0};
				el.shopsItems.each(function() {
					data.scrollerHeight += $(this).outerHeight()
				});
				if (el.shops && el.shopsMap.data('coords')) {
					data.map = new ymaps.Map('action-shops-map', {
						'center': el.shopsMap.data('coords').split(','),
						'zoom': el.shopsMap.data('zoom') || 10
					});
					data.map.controls.add('zoomControl', {left: '10px', top: '13px'});
					setTimeout(function() {
						window.shopDB.renderAll(data.map);
					}, 500);
				}
				function initShares() {
					var shareLinks = $('A', el.shareList);
					shareLinks.on('click', function() {
						var $that = $(this);
						var shareURL = $that.attr('href');
						if (shareURL) {
							window.open(shareURL, '', 'toolbar=0,status=0,width=600,height=400');
						}
						return false;
					});
				}

				if (el.shareList.length) {
					initShares();
				}
				el.shopsList.baron({scroller: '.scroller', bar: '.scroller__bar'});
				el.toggleList.on('click', function() {
					var $that = $(this), thatData = $that.data(), oldText = $that.text();
					$that.text(thatData['alt']).data('alt', oldText);
					if (!thatData['opened']) {
						$that.data('opened', true);
						el.shopsTwin.addClass('tall');
					} else {
						$that.data('opened', false);
						el.shopsTwin.removeClass('tall');
					}
				});
				el.shopsTwin.on('transitionend webkitTransitionEnd', function() {
					data.map.container.fitToViewport();
					$(window).resize();
				});
			});
		})();
		(function() {
			var target = $('BODY');
			target.each(function() {
				if (!$('.shop-page-title').length)return;
				var context = $(this), el = {
					'mapObject': $('.js-shop-page-map-object', context),
					'brandList': $('.js-shop-page-brand-list', context),
					'printPage': $('.js-shop-page-info-print', context),
					'shopAround': $('.js-shop-page-info-shoparound', context),
					'shopPolaroid': $('.js-shop-polaroid', context)
				}, data = {'dbAccessIVID': undefined};

				function mapInit() {
					var shopID = el.mapObject.data('shopId'), shopObject, staticURL = 'http://static-maps.yandex.ru/1.x/?l=map';
					data.dbAccessIVID = setInterval(function() {
						if (shopDB.db) {
							clearInterval(data.dbAccessIVID);
							shopObject = shopDB.db[shopID];
							data.map = new ymaps.Map(el.mapObject.attr('id'), {
								'center': shopObject.coords,
								'zoom': el.mapObject.data('zoom') || 10
							});
							data.map.controls.add('zoomControl', {left: '12px', top: '19px'});
							shopDB.renderAll(data.map);
							shopDB.filterMarkers(data.map, [shopID.toString()], true);
							staticURL += '&size=500,450';
							staticURL += '&z=15';
							staticURL += '&pt=' + shopObject.coords[1] + ',' + shopObject.coords[0] + ',';
							staticURL += 'pm2pnm';
							staticURL += '&lang=ru-RU';
							el.mapObject.before($('<img />', {
								'src': staticURL,
								'class': 'shop-page-map-static',
								'width': 500,
								'height': 450
							}));
						} else {
							console.info('База магазинов не загружена. Еще попытка...');
						}
					}, 500);
				}

				function brandListInit() {
					var $title = $('.js-shop-page-brand-list-title', el.brandList), $panel = $('.js-shop-page-brand-list-panel', el.brandList);
					$title.on('click', function() {
						var $that = $(this), thatData = $that.data(), oldText = $that.text();
						$that.text(thatData['alt']).data('alt', oldText);
						$panel.toggle();
					});
				}

				function printInit() {
					el.printPage.on('click', function() {
						window.print();
					});
				}

				function shopAroundInit() {
					el.shopAround.on('click', function() {
						var $that = $(this);
						var aroundShops = $that.data('around');
						if (aroundShops.toString().indexOf("|") > -1)aroundShops = aroundShops.split('|'); else
							aroundShops = [aroundShops.toString()];
						$('HTML, BODY').animate({'scrollTop': el.mapObject.offset().top - $('.l-header').height() - 30}, '400', function() {
							shopDB.filterMarkers(data.map, aroundShops, true, true);
						});
						return false;
					});
				}

				function polaroidInit() {
					var $port = $('.js-shop-polaroid-port', el.shopPolaroid), $portItems = $('A', $port);
					$portItems.fancybox({openEffect: 'elastic', closeEffect: 'elastic'});
				}

				if (el.mapObject.length)mapInit();
				if (el.brandList.length)brandListInit();
				if (el.printPage.length)printInit();
				if (el.shopAround.length)shopAroundInit();
				if (el.shopPolaroid.length)polaroidInit();
			});
		})();
	});
});